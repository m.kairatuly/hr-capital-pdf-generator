export const fetchTestResult = async (id) => {
	return fetch(`http://192.168.202.50:8080/api/results/get-conclusion/${id}`)
		.then((res) => res.json())
		.then(
			({ age, date, gender, firstName, lastName, score, percents, ...res }) => {
				return {
					result: {
						participant: {
							age,
							gender,
							firstName,
							lastName,
						},
						date: new Date(date),
					},
					percents: percents ?? { a: score },
					...res,
				};
			}
		);
};
