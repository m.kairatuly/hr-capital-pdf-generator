export const getWidth = (score, maxScore, totalScore) => {
	return `${(Math.abs(maxScore - score) / totalScore) * 100}%`;
};
