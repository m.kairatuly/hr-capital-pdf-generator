import { Layout } from './components/Layout';

function App() {
	return (
		<div
			className="App"
			style={{
				width: '100%',
				height: '100vh',
			}}
		>
			<Layout />
		</div>
	);
}

export default App;
