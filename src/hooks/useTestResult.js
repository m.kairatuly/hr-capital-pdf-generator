import { useContext } from 'react';
import { PDFGeneratorContext } from '../components/PDFGenerator';

export const useTestResult = () => {
	return useContext(PDFGeneratorContext).result;
};
