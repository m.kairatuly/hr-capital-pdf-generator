import { useContext } from 'react';
import { PDFGeneratorContext } from '../components/PDFGenerator';

export const useTestConfig = () => {
	return useContext(PDFGeneratorContext).config;
};
