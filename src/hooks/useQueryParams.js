import { useMemo } from 'react';

export const useQueryParams = () => {
	const search = window.location.search;

	const query = useMemo(() => {
		const params = new URLSearchParams(search);
		const query = {};
		for (const [key, value] of params) {
			query[key] = value;
		}
		return query;
	}, [search]);

	return query;
};
