import { Image, StyleSheet, Text, View } from '@react-pdf/renderer';
import { useTestResult } from '../hooks/useTestResult';
import { generatorStyles } from './PDFGenerator';

const styles = StyleSheet.create({
	root: {
		fontSize: 10,
	},
	title: {
		fontSize: 14,
		fontWeight: 500,
		marginBottom: 5,
	},
	warningRow: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		marginBottom: 5,
		color: '#EE2222',
		fontWeight: 500,
		textTransform: 'uppercase',
		marginTop: 10,
		fontSize: 8,
	},
	warningIcon: {
		width: 7,
		height: 7,
		marginRight: 5,
	},
});

export const CardSection = ({ title, name, description }) => {
	const { canceled } = useTestResult();
	const isCanceled = canceled[name.substring(0, 1)] === 1;

	return (
		<View
			style={{ ...generatorStyles.paperSection, ...styles.root }}
			wrap={false}
		>
			<Text style={styles.title}>{title}</Text>
			<Text>{description}</Text>

			{isCanceled && (
				<View style={styles.warningRow}>
					<Image style={styles.warningIcon} src="/note-icon.png" />

					<Text>Черта может быть ложной из-за синдрома</Text>
				</View>
			)}
		</View>
	);
};
