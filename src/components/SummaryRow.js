import { StyleSheet, Text, View } from '@react-pdf/renderer';
import { SUMMARY_ROW_HEIGHT } from '../constants';
import { useTestResult } from '../hooks/useTestResult';

const styles = StyleSheet.create({
	root: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '5 10',
		height: SUMMARY_ROW_HEIGHT,
	},
	leftSide: {
		display: 'flex',
		alignItems: 'stretch',
		flexDirection: 'row',
		width: '70%',
	},
	rightSide: {
		textAlign: 'center',
		width: '30%',
	},
	letterSquare: {
		width: 22,
		height: 22,
		backgroundColor: '#3770C7',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: 10,
		borderRadius: 5,
		fontWeight: 500,
		color: '#fff',
	},
	nameGroup: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'space-between',
	},
	name: {
		fontWeight: 500,
	},
	description: {
		color: '#67696e',
		fontSize: 7,
	},
	score: {
		fontWeight: 500,
	},
});

export const SummaryRow = ({ name, description, score, index }) => {
	const { type } = useTestResult();

	return (
		<View key={`summary-row-${name}`} style={styles.root}>
			<View style={styles.leftSide}>
				{type === 'oxford' && (
					<View style={styles.letterSquare}>
						<Text>{String.fromCharCode(65 + index)}</Text>
					</View>
				)}
				<View style={styles.nameGroup}>
					<Text style={styles.name}>{name}</Text>
					<Text style={styles.description}>{description}</Text>
				</View>
			</View>

			<View style={styles.rightSide}>
				<Text style={styles.score}>{score}</Text>
			</View>
		</View>
	);
};
