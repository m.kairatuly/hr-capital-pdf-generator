import { StyleSheet, Text, View } from '@react-pdf/renderer';
import { useMemo } from 'react';
import { SUMMARY_ROW_HEIGHT } from '../constants';
import { useTestConfig } from '../hooks/useTestConfig';
import { getWidth } from '../util/getWidth';
import { Triangle } from './Triangle';

const COLOR_TYPES_MAP = {
	1: '#60A7DB',
	2: '#D2D3D5',
	3: '#F89520',
	4: '#3566BC',
};

const COLOR_ZONES_STYLES_MAP = [
	{ backgroundColor: '#E53223', color: '#ffffff' },
	{ backgroundColor: '#F39D2D', color: '#ffffff' },
	{ backgroundColor: '#FFEC01', color: '#000000' },
	{ backgroundColor: '#C9CF1C', color: '#000000' },
];

const styles = StyleSheet.create({
	root: {
		height: SUMMARY_ROW_HEIGHT,
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		position: 'relative',
	},
	bar: {
		position: 'absolute',
		left: 0,
		height: 20,
		borderRadius: 3,
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-end',
		color: '#fff',
		padding: '0 5',
		fontWeight: 500,
	},
	stripesContainer: {
		position: 'absolute',
		left: 5,
		top: 5,
		bottom: 5,
		right: 5,
		zIndex: -1,
		display: 'flex',
		flexDirection: 'row',
		overflow: 'hidden',
	},
	stripeLine: {
		position: 'relative',
		zIndex: -1,
		width: 2,
		maxWidth: 2,
		minWidth: 2,
		height: '100%',
		marginRight: 3,
		borderRadius: 3,
		backgroundColor: '#ffffff',
	},
	score: {
		position: 'relative',
	},
	outerScoreContainer: {
		position: 'absolute',
		zIndex: 1,
		left: '100%',
		height: '100%',
		marginLeft: 10,
		top: 0,
		width: 30,
		bottom: 0,
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
	},
	outerScore: {
		color: '#000000',
		marginLeft: 2.5,
	},
	colorZonesTrack: {
		zIndex: -1,
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		height: 4,
		backgroundColor: '#D4ECFB',
	},
	colorZonesBar: {
		position: 'absolute',
		top: 0,
		left: 0,
		height: '100%',
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-end',
		paddingRight: 4,
	},
	colorZonesBarText: {
		textAlign: 'right',
		position: 'absolute',
		left: 0,
		right: 4,
		top: -0.5,
		fontSize: 4,
		fontWeight: 500,
	},
	colorZoneTriangle: {
		position: 'absolute',
		top: 0,
		right: -3.5,
	},
});

export const SummaryScore = ({ score, type, hasStripes, colorZones }) => {
	const { minScore, maxScore } = useTestConfig();

	const stripes = useMemo(() => {
		const stripes = [];

		if (!hasStripes) return stripes;

		for (let i = 0; i < Math.abs(Math.floor((100 + score) / 4)); i++) {
			stripes.push(
				<View
					key={`stripe-line-${Math.random()}`}
					style={{
						...styles.stripeLine,
					}}
				/>
			);
		}

		return stripes;
	}, [hasStripes, score, type]);

	const right = getWidth(score, maxScore, Math.abs(minScore) + maxScore);

	return (
		<View style={styles.root}>
			{colorZones && (
				<>
					<View style={styles.colorZonesTrack}>
						{colorZones.map((colorZone, index) => (
							<View
								key={`color-zone-${index}`}
								style={{
									...styles.colorZonesBar,
									zIndex: 10 + colorZones.length + index,

									...COLOR_ZONES_STYLES_MAP[index],
									right: getWidth(
										colorZone,
										maxScore,
										Math.abs(minScore) + maxScore
									),
								}}
							>
								<Text style={styles.colorZonesBarText}>{colorZone}</Text>
								<View style={styles.colorZoneTriangle}>
									<Triangle width={6} height={14} />
								</View>
							</View>
						))}
					</View>
				</>
			)}
			<View
				style={{
					...styles.bar,
					right,
					backgroundColor: COLOR_TYPES_MAP[type],
				}}
			>
				{hasStripes && <View style={styles.stripesContainer}>{stripes}</View>}

				{/* {score > -75 && <Text style={styles.score}>{score}</Text>} */}

				{/* 
				{score <= -75 && (
					<View style={styles.outerScoreContainer}>
						<Text style={styles.outerScore}>{score}</Text>
					</View>
				)} */}
			</View>
		</View>
	);
};
