import { StyleSheet, Text, View } from '@react-pdf/renderer';
import { useTestConfig } from '../hooks/useTestConfig';

const styles = StyleSheet.create({
	titleSection: {
		width: '50%',
		marginBottom: 25,
	},
	title: {
		fontSize: 24,
		fontWeight: 500,
	},
});

export const Header = () => {
	const { title } = useTestConfig();

	return (
		<View style={styles.titleSection}>
			<Text style={styles.title}>{title}</Text>
		</View>
	);
};
