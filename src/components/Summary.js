import { StyleSheet, Text, View } from '@react-pdf/renderer';
import { useMemo } from 'react';
import { MANIC_MAP, SUMMARY_ROW_HEIGHT } from '../constants';
import { useTestConfig } from '../hooks/useTestConfig';
import { useTestResult } from '../hooks/useTestResult';
import { getWidth } from '../util/getWidth';
import { generatorStyles } from './PDFGenerator';
import { SummaryRow } from './SummaryRow';
import { SummaryScore } from './SummaryScore';

const styles = StyleSheet.create({
	testSummarySection: {
		padding: 0,
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'stretch',
		fontSize: 8,
	},
	summaryLeftSide: {
		width: '45%',
		backgroundColor: '#ebf1fa',
		display: 'flex',
		flexDirection: 'column',
		height: '100%',
		padding: 0,
		borderRadius: 10,
	},
	summaryRightSide: {
		position: 'relative',
		display: 'flex',
		flexDirection: 'column',
		width: '55%',
		height: '100%',
	},
	summaryHeader: {
		height: SUMMARY_ROW_HEIGHT,
		maxHeight: SUMMARY_ROW_HEIGHT,
		width: '100%',
		borderBottom: '1 solid #ccc',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '0 10',
	},
	summaryHeaderLeftColumn: {
		width: '70%',
		paddingLeft: 32,
	},
	summaryHeaderRightColumn: {
		textAlign: 'center',
		width: '30%',
	},
	summaryProgress: {
		position: 'absolute',
		height: '100%',
		left: 10,
		right: 10,
		top: 0,
		bottom: 0,
	},
	summaryProgressLabelGroup: {
		position: 'absolute',
		top: 0,
		height: '100%',
		zIndex: 1,
		padding: '12 5',
		color: '#3566BC',
		fontWeight: 500,
	},
	summaryScores: {
		padding: '0 10',
	},
});

export const Summary = () => {
	const {
		summary,
		progressPoints,
		minScore,
		maxScore,
		columnTitles,
		colorZones,
	} = useTestConfig();
	const { percents, type, cards } = useTestResult();

	const summaryList = useMemo(() => {
		return summary.map((el, i) => ({
			...el,
			score: percents[String.fromCharCode(65 + i).toLowerCase()],
			hasStripes:
				MANIC_MAP.has(i) &&
				cards?.some((card) => card.cardName === MANIC_MAP.get(i)),
		}));
	}, [percents, summary, cards]);

	return (
		<View
			style={{ ...generatorStyles.paperSection, ...styles.testSummarySection }}
			wrap={false}
		>
			<View style={{ ...styles.paperSection, ...styles.summaryLeftSide }}>
				<View style={styles.summaryHeader}>
					<Text
						style={{
							...styles.summaryHeaderLeftColumn,
							...(type !== 'oxford' && {
								paddingLeft: 0,
							}),
						}}
					>
						{columnTitles.left}
					</Text>
					<Text style={styles.summaryHeaderRightColumn}>
						{columnTitles.right}
					</Text>
				</View>

				{summaryList.map((el, i) => (
					<SummaryRow key={`progress-row-${el.name}`} {...el} index={i} />
				))}
			</View>

			<View style={styles.summaryRightSide}>
				<View style={styles.summaryHeader}></View>

				<View style={styles.summaryProgress}>
					{progressPoints.map((point) => (
						<View
							key={`progress-point-${point.value}`}
							style={{
								...styles.summaryProgressLabelGroup,

								...(point.labelPosition === 'right'
									? {
											left: getWidth(
												point.value,
												minScore,
												maxScore + Math.abs(minScore)
											),
									  }
									: {
											right: getWidth(
												point.value,
												maxScore,
												maxScore + Math.abs(minScore)
											),
									  }),

								[`border${point.labelPosition === 'left' ? 'Right' : 'Left'}`]:
									'1px solid #eee',

								...(point.isLineDisabled && {
									borderLeft: 'none',
									paddingLeft: 0,
									paddingRight: 0,
								}),
							}}
						>
							<Text style={{ ...styles.summaryProgressLabel }}>
								{point.value}
							</Text>
						</View>
					))}
				</View>

				<View style={styles.summaryScores}>
					{summaryList.map((el, i) => (
						<SummaryScore
							key={`progress-row-bar-${el.name}`}
							score={el.score}
							type={el.type}
							hasStripes={el.hasStripes}
							colorZones={colorZones ? colorZones[i] : undefined}
						/>
					))}
				</View>
			</View>
		</View>
	);
};
