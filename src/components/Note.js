import { Image, StyleSheet, Text, View } from '@react-pdf/renderer';
import { useTestConfig } from '../hooks/useTestConfig';
import { generatorStyles } from './PDFGenerator';

const styles = StyleSheet.create({
	root: {
		fontSize: 7,
		color: '#67696e',
	},
	titleRow: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		marginBottom: 5,
		color: '#EE2222',
		fontWeight: 500,
		textTransform: 'uppercase',
	},
	titleIcon: {
		width: 7,
		height: 7,
		marginRight: 5,
	},
});

export const Note = () => {
	const { note } = useTestConfig();

	return (
		<View
			style={{ ...generatorStyles.paperSection, ...styles.root }}
			wrap={false}
		>
			<View style={styles.titleRow}>
				<Image style={styles.titleIcon} src="/note-icon.png" />
				<Text>Важное примечание</Text>
			</View>
			<Text>{note}</Text>
		</View>
	);
};
