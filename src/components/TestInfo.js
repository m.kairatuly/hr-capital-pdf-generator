import { StyleSheet, Text, View } from '@react-pdf/renderer';
import { useTestResult } from '../hooks/useTestResult';
import { generatorStyles } from './PDFGenerator';

const GENDER_MAP = {
	MALE: 'Муж.',
	FEMALE: 'Жен.',
};

const styles = StyleSheet.create({
	testInfoSection: {
		fontSize: 10,
		display: 'flex',
		flexDirection: 'row',
	},
	testInfoStack: {
		display: 'flex',
		flexDirection: 'column',
	},
	testInfoStackLeft: {
		width: '30%',
		paddingRight: 10,
	},
	testInfoStackRight: {
		width: '70%',
		paddingLeft: 10,
	},
	testInfoGroup: {
		display: 'flex',
		flexDirection: 'row',
		marginBottom: 2,
	},
	testInfoLabel: {
		color: '#b4b4b4',
		width: '50%',
	},
});

export const TestInfo = () => {
	const {
		result: { participant, date },
		vacancyName,
	} = useTestResult();

	return (
		<View
			style={{ ...generatorStyles.paperSection, ...styles.testInfoSection }}
		>
			<View style={{ ...styles.testInfoStack, ...styles.testInfoStackLeft }}>
				<View style={styles.testInfoGroup}>
					<Text style={styles.testInfoLabel}>Дата:</Text>
					<Text>
						{date.toLocaleString('ru-RU', {
							day: '2-digit',
							month: '2-digit',
							year: 'numeric',
						})}
					</Text>
				</View>
				<View style={styles.testInfoGroup}>
					<Text style={styles.testInfoLabel}>Пол:</Text>
					<Text>{GENDER_MAP[participant.gender]}</Text>
				</View>
				<View style={styles.testInfoGroup}>
					<Text style={styles.testInfoLabel}>Возраст:</Text>
					<Text>{participant.age}</Text>
				</View>
			</View>
			<View
				style={{
					...styles.testInfoStack,
					...styles.testInfoStackRight,
				}}
			>
				<View style={styles.testInfoGroup}>
					<Text style={styles.testInfoLabel}>Инициатор тестирования:</Text>
					<Text>{`${participant.firstName} `}</Text>
				</View>
				<View style={styles.testInfoGroup}>
					<Text style={styles.testInfoLabel}>Вакансия:</Text>
					<Text>{vacancyName}</Text>
				</View>
			</View>
		</View>
	);
};
