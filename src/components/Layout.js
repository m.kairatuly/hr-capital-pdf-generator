import { PDFDownloadLink, PDFViewer } from '@react-pdf/renderer';
import { useCallback, useEffect, useState } from 'react';
import { useQueryParams } from '../hooks/useQueryParams';
import { fetchTestResult } from '../util/fetchTestResult';
import { Device } from './Device';
import { PDFGenerator } from './PDFGenerator';

export const Layout = () => {
	const { id } = useQueryParams();
	const [isError, setIsError] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [testResult, setTestResult] = useState();

	const fetchData = useCallback(async () => {
		if (!id || isLoading) return;

		setIsLoading(true);
		try {
			const res = await fetchTestResult(id);
			setTestResult(res);
			setIsLoading(false);
		} catch (error) {
			setIsError(true);
		}
	}, [id, isLoading]);

	useEffect(() => {
		fetchData();

		return () => {};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	if (isLoading) {
		return (
			<div className="centered-container">
				<p>Загрузка...</p>
			</div>
		);
	}

	if (isError || !testResult) {
		return (
			<div className="centered-container">
				<p>Ошибка загрузки данных</p>
			</div>
		);
	}

	return (
		<Device>
			{({ isMobile }) =>
				isMobile ? (
					<div className="centered-container">
						<PDFDownloadLink
							className="download-button"
							document={<PDFGenerator testResult={testResult} />}
							fileName={`hr-capital-result-${testResult.result.participant.firstName}-${testResult.result.participant.lastName}.pdf`}
						>
							{({ loading }) => (loading ? 'Загрузка...' : 'Скачать PDF')}
						</PDFDownloadLink>
					</div>
				) : (
					<PDFViewer style={{ height: '100%', width: '100%' }}>
						<PDFGenerator testResult={testResult} />
					</PDFViewer>
				)
			}
		</Device>
	);
};
