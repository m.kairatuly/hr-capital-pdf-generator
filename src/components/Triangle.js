import { Path, Svg } from '@react-pdf/renderer';

export const Triangle = ({ width = 10, height = 8 }) => (
	<Svg width={width} height={height} viewBox="0 0 12 28" fill="none">
		<Path
			d="M5.567 7.25L6.00001 8L6.43302 7.25L9.89712 1.25L10.3301 0.5L9.46411 0.5L2.53591 0.5L1.66988 0.5L2.1029 1.25L5.567 7.25Z"
			fill="#000000"
			stroke="#ffffff"
		/>
	</Svg>
);
