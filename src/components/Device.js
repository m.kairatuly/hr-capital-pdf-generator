import * as rdd from 'react-device-detect';

export const Device = (props) => (
	<div className="device-layout-component">{props.children(rdd)}</div>
);
