import { Document, Font, Page, StyleSheet } from '@react-pdf/renderer';
import { createContext } from 'react';
import { GENERATOR_CONFIGS } from '../constants';
import stolzlBold from '../fonts/stolzl-bold.ttf';
import stolzlBook from '../fonts/stolzl-book.ttf';
import stolzlMedium from '../fonts/stolzl-medium.ttf';
import { CardSection } from './CardSection';
import { Header } from './Header';
import { Note } from './Note';
import { Summary } from './Summary';
import { TestInfo } from './TestInfo';

Font.register({
	family: 'Stolz',
	fonts: [
		{
			src: stolzlBook,
			fontWeight: 400,
		},
		{
			src: stolzlMedium,
			fontWeight: 500,
		},
		{
			src: stolzlBold,
			fontWeight: 700,
		},
	],
});

export const generatorStyles = StyleSheet.create({
	page: {
		padding: '50 32',
		fontFamily: 'Stolz',
		backgroundColor: '#F8F8F8',
		fontWeight: 400,
	},
	paperSection: {
		borderRadius: 10,
		backgroundColor: '#fff',
		padding: 14,
		marginBottom: 20,
	},
});

export const PDFGeneratorContext = createContext();

export const PDFGenerator = ({ testResult }) => {
	const config = GENERATOR_CONFIGS[testResult.type];

	return (
		<PDFGeneratorContext.Provider value={{ result: testResult, config }}>
			<Document>
				<Page style={generatorStyles.page}>
					<Header />
					<TestInfo />
					<Summary />
					<Note />
				</Page>
				{testResult.cards && (
					<Page style={generatorStyles.page}>
						{testResult.cards.map((card) => (
							<CardSection
								key={`card-section-${card.id}`}
								name={card.cardName}
								title={card.cardTitle}
								description={card.cardText}
							/>
						))}
					</Page>
				)}
			</Document>
		</PDFGeneratorContext.Provider>
	);
};
