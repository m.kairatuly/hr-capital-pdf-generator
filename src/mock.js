export const MOCK_TEST = {
	type: 'oxford',
	time: '01:23:45',
	cards: [
		{
			cardName: 'a1',
			cardTitle: 'a1',
			cardText:
				'Reprehenderit cillum magna labore incididunt commodo incididunt. Ut consequat Lorem exercitation est ullamco officia eiusmod do cillum eiusmod dolore. Sunt fugiat ad eiusmod minim enim proident aliquip voluptate ea dolore est non.',
		},
		{
			cardName: 'b1',
			cardTitle: 'b1',
			cardText:
				'Reprehenderit cillum magna labore incididunt commodo incididunt. Ut consequat Lorem exercitation est ullamco officia eiusmod do cillum eiusmod dolore. Sunt fugiat ad eiusmod minim enim proident aliquip voluptate ea dolore est non.',
		},
	],
	percents: {
		a: 40,
		b: -10,
		c: 45,
		d: 19,
		e: -19,
		f: 16,
		g: 100,
		h: 55,
		i: 29,
		j: -5,
	},
	canceled: {
		a: 1,
		b: 0,
		c: 1,
	},
	doubt: 12,
	result: {
		date: new Date(),
		participant: {
			firstName: 'Иван',
			lastName: 'Иванов',
			age: 25,
			gender: 'MALE',
		},
	},
};
