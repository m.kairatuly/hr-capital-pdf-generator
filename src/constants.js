export const SUMMARY_ROW_HEIGHT = 40;

export const OXFORD_SUMMARY = [
  {
    name: 'Внимание',
    description: 'Планирование',
    score: 27,
    type: '1',
  },
  {
    name: 'Стратегия',
    description: 'Позиция',
    score: -30,
    type: '1',
  },
  {
    name: 'Контроль',
    description: 'Спокойствие',
    score: 45,
    type: '1',
  },
  {
    name: 'Уверенность',
    description: 'Изменения',
    score: 19,
    type: '2',
  },
  {
    name: 'Энергия',
    description: 'Жизненная сила',
    score: -19,
    type: '3',
  },
  {
    name: 'Решительность',
    description: 'Сила намерения',
    score: 16,
    type: '3',
  },
  {
    name: 'Оборона',
    description: 'Способность держать давление',
    score: 100,
    type: '3',
  },
  {
    name: 'Тактика',
    description: 'Приоритет',
    score: 55,
    type: '4',
  },
  {
    name: 'Эмпатия',
    description: 'Теплота',
    score: 32,
    type: '4',
  },
  {
    name: 'Общение',
    description: 'Яркость',
    score: -5,
    type: '4',
  },
];

export const IQ_SUMMARY = [
  {
    name: 'Интеллект',
    description: 'Коэффициент интеллекта',
    score: 0,
    type: '4',
  },
];

export const VOSP_SUMMARY = [
  {
    name: 'Воспроизведение',
    description: 'Способность воспроизводить',
    type: '4',
  },
];

export const LEADER_SUMMARY = [
  {
    name: 'Лидерство',
    description: 'Способность распознавать источник',
    type: '4',
  },
];

export const GENERATOR_CONFIGS = {
  oxford: {
    title: 'Тест на личные качества Tool Test',
    maxScore: 100,
    minScore: -100,
    columnTitles: {
      left: 'Личностные качества',
      right: 'Показатели',
    },
    progressPoints: [
      {
        value: -100,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: -19,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 0,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 32,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 100,
        isLineDisabled: false,
        labelPosition: 'left',
      },
    ],
    colorZones: [
      [0, 30, 60, 80],
      [-50, -19, 0, 15],
      [-50, -10, 15, 50],
      [0, 30, 45, 60],
      [20, 35, 60, 80],
      [20, 30, 50, 70],
      [-50, -25, 0, 20],
      [-50, -25, 5, 35],
      [-50, -10, 20, 50],
      [-10, 10, 30, 60],
    ],
    summary: OXFORD_SUMMARY,
    note: `Взаимосвязь между точками на графике намного важнее, чем отдельное положение каждой из них. Поэтому результаты теста формируют для Вас лишь ОСНОВУ для более тщательного анализа продуктивности и потенциала Ваших кандидатов. В любом случае получите консультацию у специалиста «HR Capital»!`,
  },
  iq: {
    title: 'ТEXEC-IQ Test version A Digital Promotion.kz',
    maxScore: 155,
    minScore: 0,
    columnTitles: {
      left: 'Результат',
      right: 'Итог',
    },
    progressPoints: [
      {
        value: 0,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 100,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 155,
        isLineDisabled: false,
        labelPosition: 'left',
      },
    ],
    summary: IQ_SUMMARY,
    note: 'Интеллект не всегда напрямую связан с рациональным поведением. Высокий уровень интеллекта в сочетании с иррациональными эмоциями может привести к тому, что человек станет «гениальным разрушителем». Однако для того, чтобы успешно работать на руководящей должности, необходимо иметь достаточно высокий уровень интеллекта, чтобы быть в состоянии замечать возникающие нежелательные ситуации и находить способы справиться с ними. Чтобы лучше понять, как данный результат теста соотносится с той или иной должностью или с индивидуальностью человека, свяжитесь с консультантом «HR Capital», который сможет дать вам более подробные объяснения.',
  },
  vosp: {
    title: 'Воспроизведение Digital Promotion.kz',

    maxScore: 100,
    minScore: 0,
    columnTitles: {
      left: 'Точка',
      right: 'Оценка',
    },
    progressPoints: [
      {
        value: 0,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 50,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 100,
        isLineDisabled: false,
        labelPosition: 'left',
      },
    ],
    summary: VOSP_SUMMARY,
    note: 'Оценщик «HR Capital» выдаст Вам точный результат и его значение на отдельно оформленном листе.',
  },
  leader: {
    title: 'EXEC Leadership Analysis Digital Promotion.kz',
    maxScore: 100,
    minScore: 0,
    columnTitles: {
      left: 'Точка',
      right: 'Оценка',
    },
    progressPoints: [
      {
        value: 0,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 50,
        isLineDisabled: false,
        labelPosition: 'right',
      },
      {
        value: 100,
        isLineDisabled: false,
        labelPosition: 'left',
      },
    ],
    summary: LEADER_SUMMARY,
    note: 'Для того чтобы получить более подробную информацию о том, насколько важны полученные результаты, обращайтесь к представителю «HR Capital».',
  },
};

export const MANIC_MAP = new Map([
  [1, 'm_b'],
  [4, 'm_e'],
]);
